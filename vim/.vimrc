" _   __(_)___ ___  __________
"| | / / / __ `__ \/ ___/ ___/
"| |/ / / / / / / / /  / /__  
"|___/_/_/ /_/ /_/_/   \___/  
                            
" install vim plug
if empty(glob('~/.vim/autoload/plug.vim'))
   silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
       \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
         autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" pluggins
call plug#begin('~/.vim/plugged')
Plug 'JuliaEditorSupport/julia-vim'
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'dracula/vim',{'as':'dracula'}
call plug#end()

" relative numbers
set number
set relativenumber

" default encoding
set encoding=utf8

" enable theme 
syntax on

colorscheme dracula

let g:airline_theme='dracula'

" status line (lightline) config
set laststatus=2
  
if !has('gui_running')
    set t_Co=256
endif

let g:lightline = {
      \ 'colorscheme': 'dracula',
      \ }
	
" remove the --INSERT--
set noshowmode
